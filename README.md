# Trabajo Práctico - 1er. Parcial
## Sistema de Seguimiento de Pacientes
### Integrantes

- Francisco Candia
- Fabrizio Coscia
- Maximiliano Duré
- Mateo Fidabel

## Requerimientos

- Angular CLI: `npm install @angular/cli -g`

## Instrucciones para el desarrollador

1. Instalar las dependencias con `npm install`
2. Correr el servidor con `ng serve`
